package com.epam.de.dsa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RotateArrayTest {

    RotateArray rotateArray;

    @BeforeEach
    void setUp(){
        rotateArray = new RotateArray();
    }
    @Test
    void rotateTest(){
        int[] a ={10,20,30,40,50,60};
        int[] expected = {30,40,50,60,10,20};
        assertArrayEquals(expected,rotateArray.rotate(a,2));
        assertArrayEquals(a,rotateArray.rotate(a,0));
    }
}