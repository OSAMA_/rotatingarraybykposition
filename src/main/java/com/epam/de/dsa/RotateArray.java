package com.epam.de.dsa;

public class RotateArray {
    void reverse(int[] nums, int low, int high){
        while(low < high){
            int temp = nums[low];
            nums[low] = nums[high];
            nums[high] = temp;
            low++;
            high--;
        }
}

    public int[] rotate(int[] elements, int byNumberOfPositions) {
        byNumberOfPositions %= elements.length;
        if(byNumberOfPositions == 0) {
            return elements;
        }
        reverse(elements, 0, byNumberOfPositions - 1);
        reverse(elements, byNumberOfPositions, elements.length - 1);
        reverse(elements, 0, elements.length - 1);

        return elements;

    }
}
